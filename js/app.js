var app;
(function (window) {
	'use strict';

	var axiosApi = axios.create({
		baseURL: 'http://localhost:5000/api'
	})

	// Your starting point. Enjoy the ride!
	const filters = {
		all: (todos) => todos,
		completed: (todos) => todos.filter(x => x.completed),
		active: (todos) => todos.filter(x => !x.completed)

	}

	Vue.component('todo', {
		name: 'todo',
		template: `<input v-on:keyup.enter="addTodo" class="new-todo" placeholder="What needs to be done?" autofocus>`,
		methods: {
			addTodo: function (ev) {
				this.$emit('create-todo', ev);
			}
		}
	})

	Vue.component("todo-list", {
		props: ["todos"],
		template: `<ul class="todo-list">
					<li class="completed" hidden>
						<div class="view">
							<input class="toggle" type="checkbox" checked>
							<label>Taste JavaScript</label>
							<button class="destroy"></button>
						</div>
						<input class="edit" value="Create a TodoMVC template">
					</li>
					<li v-for="todo in todos" v-bind:class="{completed: todo.completed}">
						<div class="view">
							<input v-on:click="toggleTodo(todo)" v-bind:checked="todo.completed"  class="toggle" type="checkbox">
							<label>{{todo.title}}</label>
							<button class="destroy" v-on:click="deleteTodo(todo)"></button>
						</div>
						<input class="edit" value="Rule the web">
					</li>
				</ul>`,
		methods: {
			toggleTodo: function (todo) {
				let idx = this.todos.indexOf(todo);
				this.todos[idx].completed = !todo.completed;
			},
			deleteTodo: function (todo) {
				if (!confirm(`delete: ${todo.title}`)) return;
				let idx = this.todos.indexOf(todo);				
						this.todos.splice(idx, 1);					
			}
		}
	})


	app = new Vue({
		el: ".todoapp",
		data: {
			title: "vTodo",
			currentFilter: 'all',
			todos: [
				{ id: 1, title: 'Create TODO', completed: true },
				{ id: 2, title: 'List TODOS', completed: true },
				{ id: 3, title: 'Crete compomponents TODO', completed: false }
			]
		},
		computed: {
			filteredTodos: function () {
				return filters[this.currentFilter](this.todos);
			}
		},
		methods: {
			addTodo: function (ev) {
						this.todos.push({id: this.todos.length +1, title: ev.target.value , completed: false});
						ev.target.value = "";
			}
		},
		filters: {
			itemFilter: function (val) {
				return val == 1 ? "item" : "items";
			}
		},
		created: function () {
			console.log('created');
		}
	});

})(window);
