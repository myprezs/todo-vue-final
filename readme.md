# TodoMVC App Template

> Template used for creating [TodoMVC](http://todomvc.com) apps

![](https://github.com/tastejs/todomvc-app-css/raw/master/screenshot.png)


## Getting started

- Clone this repo and install the dependencies with [npm](https://npmjs.com) by running: `npm install`.


## License

MIT